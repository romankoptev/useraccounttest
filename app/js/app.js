angular
	.module("UserAccount", ["ui.router","angular-md5", "ngAnimate"])
	.config(function($stateProvider, $urlRouterProvider){

		$urlRouterProvider.otherwise('/login');

		$stateProvider
			.state('login',{
				url: '/login',
				controller: 'LoginController',
				templateUrl: 'views/login.html'
			})
			.state('logout',{
				url: '/logout',
				controller: 'LogoutController'
			})
			.state('register',{
				url: '/register',
				controller: 'AccountRegisterController',
				templateUrl: 'views/register.html'
			})
			.state('account',{
				abstract: true,
				authenticate: true,
				controller: 'AccountRootController',
				url: '/account',
				templateUrl: 'views/account.html'
			})
			.state('account.list',{
				url: '/list',
				authenticate: true,
				controller: 'AccountController',
				templateUrl: 'views/account-list.html'
			})
			.state('account.delete',{
				url: '/delete/{id}',
				authenticate: true,
				controller: 'AccountDeleteController',
				templateUrl: 'views/account-list.html'
			})
			.state('account.edit',{
				url: '/edit/{id}',
				authenticate: true,
				controller: 'AccountEditController',
				templateUrl: 'views/account-edit.html'
			})
			.state('account.password',{
				url: '/password/{id}',
				authenticate: true,
				controller: 'AccountPasswordController',
				templateUrl: 'views/account-password.html'
			})
			.state('account.new',{
				url: '/new',
				authenticate: true,
				controller: 'AccountNewController',
				templateUrl: 'views/account-new.html'
			})

	})
	.run(function ($rootScope, $state, LoginService) {
		$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
			if (toState.authenticate && !LoginService.isLoggedIn()) {
				// User isn’t authenticated
				$state.transitionTo("login");
				event.preventDefault();
			}
		});
	});
