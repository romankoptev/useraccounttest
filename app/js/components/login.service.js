(function() {
	"use strict";

	angular
		.module("UserAccount")
		.factory("LoginService", function($q, AccountService) {
			var service = {};

			service.Login = Login;
			service.isLoggedIn = isLoggedIn;
			service.SetCurrentUser = SetCurrentUser;
			service.GetCurrentUser = GetCurrentUser;
			service.Logout = Logout;

			function Login(username, password) {
				var deferred = $q.defer();
				var authorized = false;

				AccountService.GetByUsername(username).then(function(account) {
					if( (account!==null) && (account.password == password) ){
						authorized = true;
						SetCurrentUser(account);
					} else {
						authorized = false;
					}
					deferred.resolve(authorized);
				});

				return deferred.promise;
			}

			function SetCurrentUser(account) {
				localStorage.currentUser = JSON.stringify(account);
			}

			function GetCurrentUser() {
				var deferred = $q.defer();

				var currentUserString = localStorage.getItem('currentUser');
				deferred.resolve(JSON.parse(currentUserString));

				return deferred.promise;
			}

			function isLoggedIn() {
				var result = localStorage.getItem('currentUser');
				return result ? true : false;
			}

			function Logout() {
				localStorage.removeItem('currentUser');
			}

			return service;
		});

})();
