(function(){
	"use strict";

	angular
		.module("UserAccount")
		.controller("LoginController", function($scope, $state, md5, LoginService) {
			$scope.auth = {};
			$scope.errorMessage = "";

			$scope.submitLogin = function() {
				LoginService.Login($scope.auth.username, md5.createHash($scope.auth.password) ).then(function(result){
					if (result) {
						//$location.path('/account/list');
						$state.transitionTo("account.list");
					} else {
						$scope.errorMessage = "Login Failed!";
					}
				});
			}
		})
		.controller("LogoutController", function($scope, $state, md5, LoginService) {
			$scope.auth = {};
			$scope.errorMessage = "";
			LoginService.Logout();
			$state.transitionTo("login");
		});
})();
