(function(){
	"use strict";

	angular
		.module("UserAccount")
		.factory("AccountService", function($http, $q, $filter) {
			var service = {};

			service.GetAllAccounts = GetAllAccounts;
			service.GetByUsername = GetByUsername;
			service.GetById = GetById;
			service.DeleteAccount = DeleteAccount;
			service.SaveAccount = SaveAccount;
			service.UpdateAccount = UpdateAccount;
			service.SaveAccounts = SaveAccounts;


			return service;

			function GetAllAccounts() {
				//return $http.get('data/accounts.json');
				var deferred = $q.defer();

				var accountsString = localStorage.getItem('accounts');
				deferred.resolve(JSON.parse(accountsString));

				return deferred.promise;
			}

			function GetByUsername(username) {
				var deferred = $q.defer();
				GetAllAccounts().then(function(result) {
					var query = $filter('filter')(result, {username: username});
					var account = query.length ? query[0] : null;
					deferred.resolve(account);
				});

				return deferred.promise;
			}

			function GetById(id) {
				var deferred = $q.defer();
				GetAllAccounts().then(function(result) {
					var query = $filter('filter')(result, {id: id});
					var account = query.length ? query[0] : null;
					deferred.resolve(account);
				});

				return deferred.promise;
			}


			function DeleteAccount(id) {
				var deferred = $q.defer();

				GetAllAccounts().then(function(accounts) {

					for (var i=0; i < accounts.length; i++) {
						if (accounts[i].id==id){
							accounts.splice(i, 1);
							break;
						}
					}
					SaveAccounts(accounts);

					deferred.resolve({ success:true });
				});


				return deferred.promise;
			}

			function SaveAccount(account) {
				var deferred = $q.defer();

				var accounts = [];
				GetAllAccounts().then(function(result) {
					if (result) accounts = result;
					if(accounts.length){
						var lastAccount = accounts[accounts.length-1];
						account.id = lastAccount.id+1;
					} else {
						account.id = 0;
					}

					accounts.push(account);
					SaveAccounts(accounts);

					deferred.resolve({ success:true });
				});

				return deferred.promise;
			}

			function UpdateAccount(account) {
				var deferred = $q.defer();
				GetAllAccounts().then(function(accounts) {
					for (var i=0; i < accounts.length; i++) {
						if (accounts[i].id==account.id){
							accounts[i] = account;
							break;
						}
					}
					SaveAccounts(accounts);

					deferred.resolve({ success:true });
				});
				return deferred.promise;
			}

			function SaveAccounts(accounts) {
				localStorage.accounts = JSON.stringify(accounts);
			}
		});
})();
