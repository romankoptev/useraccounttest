(function(){
	"use strict";

	angular
		.module("UserAccount")
		.controller("AccountRootController", function($scope, $rootScope, AccountService, LoginService) {
			LoginService.GetCurrentUser().then(function(result) {
				$rootScope.currentUser = result;
			});

			$scope.setFiles = function(element) {
				$scope.$apply(function(scope) {
					console.log('files:', element.files);
					$scope.file = element.files[0];

					if ($scope.file && $scope.file.type.indexOf('image') < 0) {
						alert("invalid type");
						return;
					}
					var reader = new FileReader();

					reader.onload = function(event) {
						$scope.image_source = event.target.result;
						//$scope.imageString = event.target.result;
						$scope.account.avatar = event.target.result;
						$scope.$apply();
					}
					// when the file is read it triggers the onload event above.
					reader.readAsDataURL(element.files[0]);
				});
			};
		})

		.controller("AccountController", function($scope, $http, AccountService, LoginService) {
			AccountService.GetAllAccounts().then(function(result){
				$scope.accounts = result;
			});
		})

		.controller("AccountPasswordController", function($scope, $state, md5, AccountService, LoginService, $stateParams) {
			$scope.errorMessage = "";
			$scope.account = {};
			AccountService.GetById($stateParams.id).then(function(account){
				$scope.account = account;
			});

			$scope.submitPassword = function(isValid) {
				if ((isValid) && ($scope.account.password == md5.createHash($scope.pass.oldPassword))) {
					$scope.account.password = md5.createHash($scope.pass.newPassword);
					AccountService.UpdateAccount($scope.account).then(function(result){
						$state.transitionTo("account.list");
					});
				} else {
					$scope.errorMessage = "Wrong Password";
				}
			};
		})

		.controller("AccountEditController", function($scope, $rootScope, $state, AccountService, LoginService, $stateParams ) {
			AccountService.GetById($stateParams.id).then(function(account){
				$scope.account = account;
			});

			$scope.submitChanges = function(isValid) {
				if (isValid){
					if($scope.currentUser.id == $scope.account.id){
						LoginService.SetCurrentUser($scope.account);
						$rootScope.currentUser = $scope.account;
					}
					AccountService.UpdateAccount($scope.account).then(function(result){
						$state.transitionTo("account.list");
					});
				}
			};

			$scope.setFiles = function(element) {
				$scope.$apply(function(scope) {
					console.log('files:', element.files);
					$scope.file = element.files[0];

					if ($scope.file.type.indexOf('image') < 0) {
						alert("invalid type");
						return;
					}
					var reader = new FileReader();

					reader.onload = function(event) {
						$scope.image_source = event.target.result;
						//$scope.imageString = event.target.result;
						$scope.account.avatar = event.target.result;
						$scope.$apply();
					}
					// when the file is read it triggers the onload event above.
					reader.readAsDataURL(element.files[0]);
				});
			};
		})
		.controller("AccountNewController", function($scope, $state, md5,  AccountService, LoginService, $stateParams ) {
			$scope.account = {id:0};

			$scope.submitNew = function(isValid) {
				if(isValid){
					$scope.account.password = md5.createHash($scope.account.password);
					AccountService.SaveAccount($scope.account).then(function(result){
						$state.transitionTo("account.list");
					});
				}
			};
		})
		.controller("AccountDeleteController", function($scope, $state, AccountService, LoginService, $stateParams ) {
			var currentUser;
			LoginService.GetCurrentUser().then(function(user){
				currentUser = user;
			});
			AccountService.DeleteAccount($stateParams.id).then(function(){
				if (currentUser.id == $stateParams.id ) LoginService.Logout();
				$state.transitionTo("account.list");
			});
		})
		.controller("AccountRegisterController", function($scope, $state, AccountService, md5){
			$scope.account = {id:0};

			$scope.submitRegistration = function(isValid) {
				if(isValid){
					$scope.account.password = md5.createHash($scope.account.password);
					AccountService.SaveAccount($scope.account).then(function(result){
						$state.transitionTo("account.list");
					});
				}
			};

			$scope.setFiles = function(element) {
				$scope.$apply(function(scope) {
					console.log('files:', element.files);
					$scope.file = element.files[0];

					if ($scope.file.type.indexOf('image') < 0) {
						alert("invalid type");
						return;
					}
					var reader = new FileReader();

					reader.onload = function(event) {
						$scope.image_source = event.target.result;
						//$scope.imageString = event.target.result;
						$scope.account.avatar = event.target.result;
						$scope.$apply();
					}
					// when the file is read it triggers the onload event above.
					reader.readAsDataURL(element.files[0]);
				});
			};
		});
})();
