var path = {
    build: {
        html: 'build',
        views: 'build/views',
        css: 'build/css',
        scss: 'app/css',
        js: 'build/js',
        fonts: 'build/fonts',
        img: 'build/images'
    },
    src: {
        bower : 'app/bower',
        html: 'app/*.html',
        views: 'app/views/**/*.html',
        css: 'app/css/**/*.css',
        sass: 'app/sass/',
        js: 'app/js/*.js',
        img: 'app/images/**/*.*',
        fonts: 'app/bower/font-awesome/fonts/*.*'
    },
    watch:{
        html: 'app/*.html',
        views: 'app/views/**/*.html',
        css: 'app/css/*.css',
        sass: 'app/sass/**/*.scss',
        js: 'app/js/**/*.js',
        img: 'app/images/**/*.*',
        fonts: 'app/fonts/**/*.*',
        build: 'build/**/*.html'
    }
};

var gulp = require("gulp"),
    eol = require('gulp-eol'),
    sass = require('gulp-ruby-sass'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano');

var browserSync = require('browser-sync').create();

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
});

// HTML work
gulp.task('html', function(){
    return gulp.src(path.src.html)
        .pipe(eol())
        .pipe(useref())
        .pipe(gulpif('*.js', uglify({
             mangle: false
         })))
        .pipe(gulpif('*.css', cssnano()))
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
});

gulp.task('reload',['html'], function(){
    browserSync.reload();
    return true;
});


// SCSS  work
gulp.task('scss', function() {
    return sass( path.src.sass+'/styles.scss', {
        compass: true,
        loadPath: [
            path.src.sass,
            path.src.bower + '/bootstrap-sass/assets/stylesheets'
        ]
        })
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    .pipe( gulp.dest(path.build.scss));
});



gulp.task('fonts', function(){
    gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts));
});

gulp.task('views', function(){
    gulp.src(path.src.views)
    .pipe(gulp.dest(path.build.views));
});


// Images work
gulp.task('img', function(){
    gulp.src(path.src.img)
    .pipe(gulp.dest(path.build.img));
});

gulp.task('build', [
    'html',
    'views',
    'scss',
    'fonts',
    'img'
]);


// Watch
gulp.task('watch', function(){
    gulp.watch([path.watch.html], ['reload']);
    gulp.watch([path.watch.sass],['scss']);
    gulp.watch([path.watch.css],['reload']);
    gulp.watch([path.watch.js],['reload']);
    gulp.watch([path.watch.fonts],['fonts']);
    gulp.watch([path.watch.img],['img']);
    gulp.watch([path.watch.views],['views']);
});

// Default
gulp.task('default', ['browser-sync', 'watch', 'build']);
